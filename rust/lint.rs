// -------------------------------------------------------------------
//
// Book: https://doc.rust-lang.org/rustc/lints/index.html
//
// # Lint Groups
//
#![forbid(future_incompatible)]
#![forbid(nonstandard_style)]
#![forbid(rust_2018_compatibility)]
#![forbid(rust_2018_idioms)]
#![forbid(unused)]
//
// # Deny by Default Lints
//
#![forbid(arithmetic_overflow)]
#![forbid(incomplete_include)]
#![forbid(ineffective_unstable_trait_impl)]
#![forbid(mutable_transmutes)]
#![forbid(no_mangle_const_items)]
#![forbid(overflowing_literals)]
#![forbid(unconditional_panic)]
#![forbid(unknown_crate_types)]
#![forbid(useless_deprecated)]
//
// # Warn by Default Lints
//
#![forbid(asm_sub_register)]
#![forbid(bindings_with_variant_name)]
#![forbid(clashing_extern_declarations)]
#![forbid(confusable_idents)]
#![forbid(const_item_mutation)]
#![forbid(deprecated)]
#![forbid(drop_bounds)]
#![forbid(exported_private_dependencies)]
#![forbid(function_item_references)]
#![forbid(improper_ctypes)]
#![forbid(improper_ctypes_definitions)]
#![forbid(incomplete_features)]
#![forbid(inline_no_sanitize)]
#![forbid(invalid_value)]
#![forbid(irrefutable_let_patterns)]
#![forbid(mixed_script_confusables)]
#![forbid(no_mangle_generic_items)]
#![forbid(non_fmt_panic)]
#![forbid(non_shorthand_field_patterns)]
#![forbid(overlapping_range_endpoints)]
#![forbid(renamed_and_removed_lints)]
#![forbid(stable_features)]
#![forbid(temporary_cstring_as_ptr)]
#![forbid(trivial_bounds)]
#![forbid(type_alias_bounds)]
#![forbid(uncommon_codepoints)]
#![forbid(unconditional_recursion)]
#![forbid(unknown_lints)]
#![forbid(unnameable_test_items)]
#![forbid(unused_comparisons)]
#![forbid(while_true)]
//
// # Allowed by Default Lints
//
#![forbid(macro_use_extern_crate)]
#![forbid(meta_variable_misuse)]
#![forbid(missing_abi)]
#![forbid(missing_copy_implementations)]
#![forbid(missing_debug_implementations)]
#![forbid(missing_docs)]
#![forbid(non_ascii_idents)]
#![forbid(noop_method_call)]
#![forbid(single_use_lifetimes)]
#![forbid(trivial_casts)]
#![forbid(trivial_numeric_casts)]
#![forbid(unaligned_references)]
#![forbid(unreachable_pub)]
#![forbid(unsafe_code)]
#![forbid(unsafe_op_in_unsafe_fn)]
#![forbid(unused_crate_dependencies)]
#![forbid(unused_import_braces)]
#![forbid(unused_lifetimes)]
#![forbid(unused_results)]
#![forbid(variant_size_differences)]
#![deny(unused_qualifications)]
//
// # Clippy lints
//
#![forbid(clippy::correctness)]
#![forbid(clippy::style)]
#![forbid(clippy::complexity)]
#![forbid(clippy::perf)]
#![forbid(clippy::pedantic)]
#![forbid(clippy::nursery)]
#![forbid(clippy::cargo)]
// -------------------------------------------------------------------
