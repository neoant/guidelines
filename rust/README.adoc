= Guidelines for The Rust Programming Language
The NeoAnt Project Developers

== Cargo.toml

Use link:Cargo.toml[Cargo.toml] as a starting point.

== Linter

Use link:lint.rs[lint.rs] as a starting point.

